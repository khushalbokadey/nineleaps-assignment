-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: nineleaps
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `reportingManager` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1, 'Anchal', 'CEO', NULL, NOW(), NOW()),(2, 'Anika', 'COO', 1, NOW(), NOW()),(3, 'Udit', 'Accounts Manager', 2, NOW(), NOW()),(4, 'Yatiyasa', 'Business Analyst', 2, NOW(), NOW()),(5, 'Zena', 'Scrum Master', 2, NOW(), NOW()),(6, 'Aahna', 'Head (Quality)', 2, NOW(), NOW()),(7, 'Yashraj', 'Quality Manager', 5, NOW(), NOW()),(8, 'Veena', 'Tester', 7, NOW(), NOW()),(9, 'Torsha', 'Mobile Tester', 7, NOW(), NOW()),(10, 'Ruchita', 'Tester', 7, NOW(), NOW()),(11, 'Oindrila', 'Mobile Tester', 7, NOW(), NOW()),(12, 'Madhulika', 'Head (Development)', 2, NOW(), NOW()),(13, 'Kimaya', 'Developer', 12, NOW(), NOW()),(14, 'Indu', 'Developer', 12, NOW(), NOW()),(15, 'Gayatri', 'Developer', 12, NOW(), NOW()),(16, 'Ashlesha', 'VP Sales', 1, NOW(), NOW()),(17, 'Hardik', 'Manager Sales', 16, NOW(), NOW()),(18, 'Birju', 'VP Marketing', 1, NOW(), NOW()),(19, 'Jaganmay', 'Manager Marketing', 18, NOW(), NOW()),(20, 'Devak', 'Head (HR)', 1, NOW(), NOW()),(21, 'Lakhan', 'Recruitment Manager', 20, NOW(), NOW()),(22, 'Naamdev', 'L&D Manager', 20, NOW(), NOW()),(23, 'Pradosh', 'Facilities', 20, NOW(), NOW()),(24, 'Phani', 'Head (Finance)', 1, NOW(), NOW()),(25, 'Sachit', 'CTO', 1, NOW(), NOW()),(26, 'Ekaa', 'Solution Architect', 25, NOW(), NOW()),(27, 'Chitral', 'Solution Architect', 25, NOW(), NOW());
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on NOW()