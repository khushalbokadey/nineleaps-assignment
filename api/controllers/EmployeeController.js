/**
 * EmployeeController
 *
 * @description :: Server-side logic for managing employees
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {

  list: function(req, res) {

    Employee.find()
      .populate('reportingManager')
      .exec(function(err, employees) {

        if (req.query.type === 'tree') {

          var data = CommonService.createHierarchyStructure(employees, null);
          if (req.wantsJSON) {
            return res.json(data)
          }
          return res.view('tree');

        } else if (req.query.type === 'table') {
          return res.view('table', {
            employees: employees
          });
        } else {
          return res.view('create', {
            employees: employees
          });
        }
      });
  },

  create: function(req, res) {
    var params = {
      name: req.body.name,
      designation: req.body.designation,
      reportingManager: req.body.reportingManager
    }
    Employee.create(params)
      .exec(function(err, employee) {
        return res.redirect('/employees?type=table');
      });
  },

};
