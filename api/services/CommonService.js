module.exports = {

  createHierarchyStructure: function(arr, parent) {
    var out = []
    for (var i in arr) {
      if (arr[i].reportingManager && arr[i].reportingManager.id == parent) {
        var children = CommonService.createHierarchyStructure(arr, arr[i].id)

        if (children.length) {
          arr[i].children = children
        }
        out.push(arr[i])
      }
      if (!arr[i].reportingManager && !parent) {
        var children = CommonService.createHierarchyStructure(arr, arr[i].id)

        if (children.length) {
          arr[i].children = children
        }
        out.push(arr[i])
      }
    }
    return out;
  }
}
