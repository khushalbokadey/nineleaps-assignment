$.ajax({
  url: '/employees?type=tree',
  type: 'GET',
  success: function(data) {
    $('#tree1')
      .tree({
        data: data,
        autoOpen: true
      });
  }
});
